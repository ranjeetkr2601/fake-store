function fetchData(url){
    fetch(url)
    .then((response)=>{
        if(response.ok){
            return response.json();
        }
        else {
            throw new Error("Fetch Failed");
        }
    })
    .then((data) => {
        if(Array.isArray(data)){
            removeLoader();
            allProducts(data);
        }
        else {
            throw new Error("Incorrect data format")
        }
    })
    .catch((err) => {
        removeLoader();

        let mainContainer = document.querySelector(".main-container");
        mainContainer.innerText = "Internal Server Error";
        
        console.error(err);
    });
}

//functions
function removeLoader(){
    let loader = document.querySelector(".lds-ellipsis");
    loader.remove();
}

function allProducts(products){
    let allProductsHeading = document.createElement("h2");
    allProductsHeading.innerText = "All Products"

    let allProductsContainer = document.createElement("div");
    allProductsContainer.className = "products-container";

    let mainContainer = document.querySelector(".main-container");

    mainContainer.appendChild(allProductsHeading);
    mainContainer.appendChild(allProductsContainer);


    let allProductsArr = products.map(fetchedProduct => {
        let productContainer = document.createElement("div");
        productContainer.className = "product";

        let productImg = document.createElement("img");
        productImg.className = "product-image"
        productImg.src = fetchedProduct.image;
        productImg.alt = fetchedProduct.title + " image";

        let productTitle = document.createElement('h3');
        productTitle.className = "product-title";
        productTitle.innerText = fetchedProduct.title;

        let productPrice = document.createElement("span");
        productPrice.className = "price";
        productPrice.innerHTML = `<span class="dollar">&dollar;</span> ${fetchedProduct.price}`;
        
        let productDescription = document.createElement("ul");
        productDescription.className = "product-description";
        productDescription.append(...descriptionSplit(fetchedProduct.description));

        let productCategory = document.createElement("span");
        productCategory.className = "product-category";
        productCategory.innerText = `Category: ${fetchedProduct.category}`;

        let {rate, count} = fetchedProduct.rating;
        let productRating = document.createElement("span");
        productRating.className = "rating";
        
        let starIcon = document.createElement('i');
        starIcon.setAttribute("class", "fa-solid fa-star");

        let ratingCount = document.createElement("span");
        ratingCount.innerText = rate;

        let userIcon = document.createElement('i');
        userIcon.setAttribute("class", "fa-solid fa-user");

        let userCount = document.createElement("span");
        userCount.innerText = count;

        productRating.append(starIcon, ratingCount, userIcon, userCount);


        let addCart = document.createElement('button');
        addCart.className = "cart-button";
        addCart.innerText = "Add to Cart "
        let cartIcon = document.createElement('i');
        cartIcon.setAttribute("class", "fa-solid fa-cart-plus");

        addCart.appendChild(cartIcon);

        productContainer.appendChild(productImg);
        productContainer.appendChild(productTitle);
        productContainer.appendChild(productCategory);
        productContainer.appendChild(productRating);
        productContainer.appendChild(productPrice);
        productContainer.appendChild(productDescription);
        productContainer.appendChild(addCart);

        return productContainer;
    });

    allProductsContainer.append(...allProductsArr);
}

function descriptionSplit(description){
    return description.split(",").reduce((acc, currDes) => {
        let des = document.createElement("li");
        des.innerText= currDes;
        acc.push(des);
        return acc;
    }, []);
}

function checkUser(){
    if(localStorage.getItem("currentUser")){
        let userDetails = JSON.parse(localStorage.getItem("currentUser"));
        let username = userDetails.first_name + " " + userDetails.last_name;

        document.querySelector("header .buttons").remove();

        let userDiv = document.createElement('div');
        userDiv.className = "buttons";

        let user = document.createElement('span');
        user.innerText = `Welcome! ${username}`;
        userDiv.appendChild(user);
        
        let logoutBtn = document.createElement('button');
        logoutBtn.classList.add("button", "logout");
        logoutBtn.innerText = "Log out";
        userDiv.appendChild(logoutBtn);

        document.querySelector("header").appendChild(userDiv);

        let mobileMenu = document.querySelector(".mobile-menu");
        let buttons = document.querySelectorAll(".mobile-menu .button");
        buttons[0].remove();
        buttons[1].remove();

        let userLi = document.createElement("li");
        userLi.appendChild(user.cloneNode(true));

        let logoutBtnLi = document.createElement("li");
        logoutBtnLi.appendChild(logoutBtn.cloneNode(true));

        mobileMenu.append(userLi, logoutBtnLi);
    }
    else{
        return;
    }
}
checkUser();

function logout(){
    let logoutBtn = document.querySelectorAll(".logout");
    
    if(logoutBtn != null){
        logoutBtn.forEach((currButton) => {
            currButton.addEventListener('click', (event) => {
                event.preventDefault();
                localStorage.clear();
                location.reload();
            });
        });
    }
    else {
        return;
    }
}
logout();

//Calling fetch function
fetchData("https://fakestoreapi.com/products");