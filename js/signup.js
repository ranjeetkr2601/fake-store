function validateForm(){
    let data = validationCheck();
    if(data){
        localStorage.setItem("currentUser", JSON.stringify(data));
        return true;
    }
    else {
        return false;
    }
}

function displayError(field, errMsg){
    removeErr(field);
    let err = document.createElement("span");
    err.innerText = errMsg;
    err.className = "errMessage";
    field.parentElement.appendChild(err);
}

function removeErr(field){
    let msgExists = document.querySelector(`.${field.parentElement.className.split(" ")[0]} .errMessage`);
    if(msgExists !== null){
        console.log("yes");
        msgExists.remove();
    }
    else{
        return;
    }
}

function onChange(field){
    removeErr(field);
}

function validationCheck(){
    let fname = document.querySelector("#first-name");
    let lname = document.querySelector("#last-name");
    let email = document.querySelector('#email');
    let password = document.querySelector("#password");
    let rePassowrd = document.querySelector("#re-password");

    let nameCheck = /^[a-zA-Z]{2,30}$/;

    let validated = true;

    if(!nameCheck.test(fname.value)){
        displayError(fname, "Name cannot include Special Characters and Numbers");
        validated = false;
    } else{
        removeErr(fname);
    }

    if(!nameCheck.test(lname.value)){
        displayError(lname, "Name cannot include Special Characters and Numbers");
        validated = false;
    } else{
        removeErr(lname);
    }
    
    if(password.value.trim() === ""){
        displayError(password, "Password cannot be empty");
        validated = false;
    } else{
        removeErr(password);
    }
    
    if(password.value !== rePassowrd.value){
        displayError(rePassowrd, "Passwords does not match");
        validated = false;
    } else {
        removeErr(rePassowrd);
    }

    if(password.value.length < 8){
        displayError(password, "Minimum password length should be 8");
        validated = false;
    } else{
        removeErr(password);
    }

    if(validated){
        return {
            first_name:fname. value,
            last_name:lname. value,
            email:email.value
        }
    }
    else {
        return null;
    }
}